import React, { Component } from 'react';

import UserScene from './components/UserScene/UserScene';

class App extends Component {
  render() {
    return (
      <div>
        <UserScene />
      </div>
    );
  }
}

export default App;
