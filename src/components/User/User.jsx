import React, { Component } from "react";
import PropTypes from 'prop-types';

import styles from "./User.module.css";

class User extends Component {
    state = {
        className: styles.draggable
    };

    addStyle(){
        const className = this.state.className + ' ' + styles.activeShadow;
        this.setState({className: className});
    }

    clearStyle(){
        const className = styles.draggable;
        this.setState({className: className});
    }

    onMouseDown(event) {
        this.props.onMouseDown(this, this.props.user);
    }

    render() {
        const user = this.props.user;
        return(
            <div className={this.state.className} onMouseDown={this.onMouseDown.bind(this)}>
                {user.name}
            </div>
        )
    }
}

User.propTypes = {
    user: PropTypes.object.isRequired,
    onMouseDown: PropTypes.func.isRequired
};

export default User;
