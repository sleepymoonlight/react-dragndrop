import React, {Component} from 'react';
import PropTypes from 'prop-types';

import styles from './DraggableUser.module.css';

class DraggableUser extends Component{
    render() {
        const user = this.props.user;
        let position = {
            left: this.props.left + "px",
            top: this.props.top + "px"
        };
        return(
            <div className={styles.activeItem} style={position}>
                {user.name}
            </div>
        )
    }
}

DraggableUser.propTypes = {
    user: PropTypes.object.isRequired,
    left: PropTypes.number.isRequired,
    top: PropTypes.number.isRequired
};

export default DraggableUser;
