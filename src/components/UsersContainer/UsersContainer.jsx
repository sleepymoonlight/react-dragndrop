import React, {Component} from 'react';
import PropTypes from 'prop-types';

import User from '../User/User';
import Title from '../Title/Title';

import styles from './UsersContainer.module.css';

class UsersContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            persons: props.persons || [],
            initial: false
        }
    }

    componentDidUpdate() {
        if (!this.state.initial && this.props.persons) {
            this.setState({persons: this.props.persons, initial: true});
        }
    }

    highlight(item) {
        if(item.addStyle){
            item.addStyle();
        }
    }

    onMouseDown(item, person) {
        this.props.onMouseDown(item, person, this);
    }

    render() {
        return (
            <div className={styles.box}>
                {(this.props.todoPerson) ? <Title>Chosen person</Title> : <Title>Available person</Title>}
                {this.state.persons.map(
                    person => <User user={person} key={person.id} onMouseDown={this.onMouseDown.bind(this)}/>)}
            </div>
        );
    }
}

UsersContainer.propTypes = {
    persons: PropTypes.array,
    onMouseDown: PropTypes.func.isRequired,
    todoPerson: PropTypes.bool.isRequired
};


export default UsersContainer;
