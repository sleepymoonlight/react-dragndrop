import React, {Component} from 'react';
import axios from 'axios';

import UsersContainer from '../UsersContainer/UsersContainer';
import DraggableUser from '../DraggableUser/DraggableUser';
import Button from '../Button/Button';

import styles from './UserScene.module.css';

class UserScene extends Component {
    state = {
        persons: [],
        currentItem: null,
        draggableItem: null,
        boxTarget: null,
        boxRoot: null,
        currentPerson: null,
        mouseOffset: {
            x: 0,
            y: 0,
        }
    };

    componentDidMount() {
        axios.get('https://jsonplaceholder.typicode.com/users').then(res => {
            const persons = res.data;
            this.setState({persons});
        });
    }

    onMouseDown(item, person, boxRoot) {
        if (boxRoot.highlight) {
            boxRoot.highlight(item);
        }
        this.setState({currentItem: item, boxRoot: boxRoot, currentPerson: person});
    }

    onMouseMove() {

    }

    render() {
        return (
            <React.Fragment>
                <Button>Submit</Button>
                <div
                    onMouseMove={this.onMouseMove.bind(this)}
                    className={styles.container}>
                    <UsersContainer persons={this.state.persons}
                                    onMouseDown={this.onMouseDown.bind(this)}
                                    todoPerson={false}/>
                    <UsersContainer onMouseDown={this.onMouseDown.bind(this)}
                                    todoPerson={true}/>
                    {(this.state.currentItem)
                        ? <DraggableUser user={this.state.currentPerson} left={0} top={0}/>
                        : <div/>}
                </div>
            </React.Fragment>
        );
    }
}

export default UserScene;
